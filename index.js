function xapSep3So() {
  num1 = document.getElementById("txt-num-1_1").value * 1;
  num2 = document.getElementById("txt-num-1_2").value * 1;
  num3 = document.getElementById("txt-num-1_3").value * 1;
  if (num3 <= num2 && num2 <= num1) {
    result = `${num3} < ${num2} < ${num1}`;
  } else if (num2 <= num3 && num3 <= num1) {
    result = `${num2} < ${num3} < ${num1}`;
  } else if (num3 <= num1 && num1 <= num2) {
    result = `${num3} < ${num1} < ${num2}`;
  } else if (num1 <= num3 && num3 <= num2) {
    result = `${num1} < ${num3} < ${num2}`;
  } else if (num2 <= num1 && num1 <= num3) {
    result = `${num2} < ${num1} < ${num3}`;
  } else {
    result = `${num1} < ${num2} < ${num3}`;
  }
  document.getElementById("result_1").innerHTML = result;
}

function guiLoiChao() {
  select = document.getElementById("txt-thanh-vien").value;
  switch (select) {
    case "B":
      document.getElementById("result_2").innerHTML = "Chào Bố";
      break;
    case "M":
      document.getElementById("result_2").innerHTML = "Chào Mẹ";
      break;
    case "A":
      document.getElementById("result_2").innerHTML = "Chào Anh";
      break;
    case "E":
      document.getElementById("result_2").innerHTML = "Chào Em";
      break;
    default:
      document.getElementById("result_2").innerHTML = "Chào Người Lạ";
  }
}

function demSoChanLe() {
  num1 = document.getElementById("txt-num-3_1").value * 1;
  num2 = document.getElementById("txt-num-3_2").value * 1;
  num3 = document.getElementById("txt-num-3_3").value * 1;
  count = 0;
  if (num1 % 2 == 0) {
    count++;
  }
  if (num2 % 2 == 0) {
    count++;
  }
  if (num3 % 2 == 0) {
    count++;
  }
  remaining = 3 - count;
  document.getElementById(
    "result_3"
  ).innerHTML = `Có ${count} số chẵn & ${remaining} số lẻ`;
}

function xacDinhTamGiac() {
  sideA = document.getElementById("txt-side-a").value * 1;
  sideB = document.getElementById("txt-side-b").value * 1;
  sideC = document.getElementById("txt-side-c").value * 1;

  if (sideA == sideB && sideB == sideC && sideA == sideC) {
    result = "Tam giác đều";
  } else if (sideA == sideB || sideB == sideC || sideA == sideC) {
    result = "Tam giác cân";
  } else if (
    Math.pow(sideA, 2) == Math.pow(sideB, 2) + Math.pow(sideC, 2) ||
    Math.pow(sideB, 2) == Math.pow(sideA, 2) + Math.pow(sideC, 2) ||
    Math.pow(sideC, 2) == Math.pow(sideA, 2) + Math.pow(sideB, 2)
  ) {
    result = "Tam giác vuông";
  } else {
    result = "Tam giác chưa xác định";
  }
  document.getElementById("result_4").innerHTML = "Đây là: " + result;
}
